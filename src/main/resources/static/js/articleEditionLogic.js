var buttonplus = document.getElementById("btnAppend");
buttonplus.addEventListener("click", function() {
	addRef();
});

function addRef(content = '') {
	const currentCnt = Number(document.getElementById("nbrRef").value) + 1;

	var refBtn = document.createElement("button");
	refBtn.id = "ref-" + currentCnt;
	refBtn.type = "button";
	refBtn.className = "flairToDisplay btn btn-primary";
	refBtn.onclick = function() {
		addLinkCiteRef(refBtn.id);
	}
	refBtn.textContent = "[" + currentCnt + "]";

	var removeBtn = document.createElement("button");
	removeBtn.id = "removebtn-" + currentCnt;
	removeBtn.type = "button";
	removeBtn.className = "btn btn-light removeButton";
	removeBtn.onclick = function() {
		removeRef(removeBtn.id);
	}
	removeBtn.textContent = "-";

	var btnsDiv = document.createElement("div");
	btnsDiv.className = "col-lg-1 btn-group-vertical form-group";

	btnsDiv.appendChild(refBtn);
	btnsDiv.appendChild(removeBtn);

	var contentTextArea = document.createElement("textarea");
	contentTextArea.className = "form-control refInput summernote";
	contentTextArea.name = "reference" + currentCnt;

	var txtDiv = document.createElement("div");
	txtDiv.className = "col-lg form-group";

	txtDiv.appendChild(contentTextArea);

	var refDiv = document.createElement("div");
	refDiv.className = "form-group row deletable";
	refDiv.id = "remv" + currentCnt;

	refDiv.appendChild(btnsDiv);
	refDiv.appendChild(txtDiv);

	document.getElementById("refs").appendChild(refDiv);
	document.getElementById("nbrRef").value = "" + currentCnt;
	
	// Setup summernote for new reference
	$(document).ready(function() {
		$('.summernote').eq(currentCnt).html(content);
		$('.summernote').eq(currentCnt).summernote();
	});
}

function removeRef(removeBtnId) {
	const numberToRemove = removeBtnId.split("-")[1];
	const identifiant = "remv" + numberToRemove;
	var toDelete = document.getElementById(identifiant);
	toDelete.outerHTML = "";
	const newNbrRef = Number(document.getElementById("nbrRef").value) - 1;
	document.getElementById("nbrRef").value = newNbrRef;
	if (newNbrRef > 0) {
		renumberAll();
	}
}

// Update the index used by attributes
function renumberAll() {
	const refDivs = Array.from(document.getElementsByClassName("deletable"));
	for (var j = 0; j < refDivs.length; ++j) {
		refDivs[j].id = "remv" + (j+1);
	}

	const flairButtons = document.getElementsByClassName("flairToDisplay");
	for (var j = 0; j < flairButtons.length; ++j) {
		var currentButton = flairButtons[j];
		currentButton.id = "ref-" + (j+1);
		currentButton.innerHTML = "[" + (j+1) + "]";
	}

	const removeButtons = document.getElementsByClassName("removeButton");
	for (var j = 0; j < removeButtons.length; ++j) {
		removeButtons[j].id = "removebtn-" + (j+1);
	}

	const refInputs = document.getElementsByClassName("refInput");
	for (var j = 0; j < refInputs.length; ++j) {
		refInputs[j].name = "reference" + (j+1);
	}
}

// Add the link of a reference to
// the article's content textarea.
function addLinkCiteRef(buttonId) {
	const refIndex = buttonId.split("-")[1];
	var refLinkTag = document.createElement("a");
	refLinkTag.id = "citeRef" + refIndex;
	refLinkTag.href = "#defRef" + refIndex;
	refLinkTag.innerHTML = "[" + refIndex + "]";
	$('.summernote[name="content"]').summernote('insertNode', refLinkTag);
}

// Dynamically create input for the event article type
const typeArticle = document.getElementById("inputType");
typeArticle.addEventListener("change", function () {
	var checkEventDateDiv =  document.getElementById('eventDateDiv');
	const isDivNotDefined = typeof(checkEventDateDiv) == 'undefined' || checkEventDateDiv == null;
	if (isDivNotDefined && typeArticle.value === "4") {
		var eventDateInput = document.createElement("input");
		eventDateInput.id = "eventDate";
		eventDateInput.name = "eventDate";
		eventDateInput.type = "date";
		eventDateInput.className = "form-control";
		eventDateInput.placeholder = "Date de l'évènement";
		
		var eventDateLabel = document.createElement("label");
		eventDateLabel.setAttribute("for", "eventDate");
		eventDateLabel.innerHTML = "Date de l'évènement";
		
		var eventDateDiv = document.createElement("div");
		eventDateDiv.id = "eventDateDiv";
		eventDateDiv.className = "form-group";
		
		eventDateDiv.appendChild(eventDateLabel);
		eventDateDiv.appendChild(eventDateInput);
		
		const inputTypeDiv = document.getElementById("inputTypeDiv");
		
		inputTypeDiv.parentNode.insertBefore(eventDateDiv, inputTypeDiv.nextSibling);
	} else {
		if (!isDivNotDefined) {
			checkEventDateDiv.parentNode.removeChild(checkEventDateDiv);
		}
	}
});