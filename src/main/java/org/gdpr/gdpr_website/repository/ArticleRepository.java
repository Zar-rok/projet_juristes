package org.gdpr.gdpr_website.repository;

import java.util.List;
import java.util.Optional;

import org.gdpr.gdpr_website.entity.Article;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ArticleRepository extends CrudRepository<Article, Long> {
    public Optional<Article> findBySlug(String slug);
    public List<Article> findAllByOrderByPublicationDateDesc();
    public Optional<Article> findByTypeAndCategory(String type, String category);
    public List<Article> findAllByTitleIgnoreCaseContaining(String title);
}
