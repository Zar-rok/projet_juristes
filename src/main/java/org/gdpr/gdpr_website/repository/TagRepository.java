package org.gdpr.gdpr_website.repository;

import java.util.List;
import java.util.Optional;

import org.gdpr.gdpr_website.entity.Tag;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TagRepository extends CrudRepository<Tag, Long> {
    public Optional<Tag> findByDescription(String description);
    public List<Tag> findAllByDescriptionIgnoreCaseContaining(String description);
}
