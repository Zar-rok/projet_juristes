package org.gdpr.gdpr_website.repository;

import java.util.Optional;

import org.gdpr.gdpr_website.entity.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {
	Optional<User> findById(Long Id);
	Optional<User> findByLastnameAndFirstname(String lastname, String firstname);
	Optional<User> findByLastnameAndFirstnameIgnoreCase(String lastname, String firstname);
}
