package org.gdpr.gdpr_website.repository;

import java.util.Optional;


import org.gdpr.gdpr_website.entity.Glossary;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GlossaryRepository extends CrudRepository<Glossary, Long> {

}
