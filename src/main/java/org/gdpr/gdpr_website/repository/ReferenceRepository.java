package org.gdpr.gdpr_website.repository;

import org.gdpr.gdpr_website.entity.Reference;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReferenceRepository  extends CrudRepository<Reference, Long> {
}
