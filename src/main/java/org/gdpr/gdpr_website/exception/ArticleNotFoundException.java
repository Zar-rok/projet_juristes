package org.gdpr.gdpr_website.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class ArticleNotFoundException extends RessourceNotFoundException {
    public ArticleNotFoundException(String slug) {
        super("Unkown article for the slug : " + slug);
    }
}
