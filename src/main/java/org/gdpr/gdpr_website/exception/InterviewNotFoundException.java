package org.gdpr.gdpr_website.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class InterviewNotFoundException extends RuntimeException {
    public InterviewNotFoundException(String slug) {
        super("Unkown interview for the slug : " + slug);
    }
}
