package org.gdpr.gdpr_website.controller;

import org.gdpr.gdpr_website.entity.Article;
import org.gdpr.gdpr_website.entity.Tag;
import org.gdpr.gdpr_website.manager.IArticleManagement;
import org.gdpr.gdpr_website.manager.ITagManagement;
import org.gdpr.gdpr_website.service.PrepareUIData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class SearchController {

    @Autowired
    IArticleManagement articleManager;

    @Autowired
    private ITagManagement tagManager;

    @Autowired
    PrepareUIData prepareUIData;

    @RequestMapping("/recherche")
    public String searchResult(
            @RequestParam(value = "search", required = false) String search,
            Model model
    ) {
        if (search == null) {
            model.addAttribute("msg", "Veuillez renseigner votre recherche.");
        } else {
            List<Article> allArticles = this.articleManager.findAllByTitleIgnoreCaseContaining(search);
            List<Tag> allTags = this.tagManager.findAllByDescriptionIgnoreCaseContaining(search);
            if (allArticles.isEmpty() && allTags.isEmpty()) {
                model.addAttribute("msg", "Aucune résultat trouvés.");
            } else {
                model.addAttribute("tags", allTags);
                model.addAttribute("articles", allArticles);
                model.addAttribute("articlesContentPreview", this.prepareUIData.buildPreview(allArticles));
            }
        }
        model.addAttribute("pageTitle", "Recherche");
        return "searchResults";
    }
}
