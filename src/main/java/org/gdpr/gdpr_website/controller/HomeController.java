package org.gdpr.gdpr_website.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {
    @RequestMapping("/")
    public String tmp() {
        // TODO : Temporary
        return "redirect:/article/actualites";
    }
    
    @RequestMapping("/wip")
    public String wip() {
        // TODO : Temporary
        return "wip";
    }
}
