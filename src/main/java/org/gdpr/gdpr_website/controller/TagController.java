package org.gdpr.gdpr_website.controller;

import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.gdpr.gdpr_website.entity.Tag;
import org.gdpr.gdpr_website.exception.RessourceNotFoundException;
import org.gdpr.gdpr_website.manager.ITagManagement;
import org.gdpr.gdpr_website.service.PrepareUIData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/etiquette")
public class TagController {
    
    private static final Logger LOGGER = Logger.getLogger(TagController.class.getName());
    
    @Autowired
    private ITagManagement tagManager;

    @Autowired
    PrepareUIData prepareUIData;
    
    @ExceptionHandler(RessourceNotFoundException.class)
    public String handleTagNotFoundException(Model model) {
        model.addAttribute("pageTitle", "Tag inconnu");
        model.addAttribute("errorHeader", "Tag inconnu");
        model.addAttribute("errorMsg", "Le tag recherché n'existe pas !");
        return "notFound";
    }
    
    @RequestMapping("/voir/{description}")
    public String displayRelatedArticles(@PathVariable(value="description") String tagDescription, Model model) {
        Optional<Tag> optTag = this.tagManager.findByDescription(tagDescription);
        if (optTag.isPresent()) {
            Tag tag = optTag.get();
            model.addAttribute("articles", tag.getArticles());
            model.addAttribute("articlesContentPreview", this.prepareUIData.buildPreview(tag.getArticles()));
            return "tagsArticle";
        } else {
            RessourceNotFoundException rnf = new RessourceNotFoundException("tag", tagDescription);
            LOGGER.log(Level.WARNING, rnf.getMessage(), rnf);
            throw rnf;
        }
    }
    
}
