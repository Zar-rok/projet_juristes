package org.gdpr.gdpr_website.controller;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.gdpr.gdpr_website.entity.Article;
import org.gdpr.gdpr_website.entity.Tag;
import org.gdpr.gdpr_website.entity.User;
import org.gdpr.gdpr_website.manager.IArticleManagement;
import org.gdpr.gdpr_website.manager.IUserManagement;
import org.gdpr.gdpr_website.service.PrepareUIData;
import org.gdpr.gdpr_website.exception.ArticleNotFoundException;
import org.gdpr.gdpr_website.exception.InterviewNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class ArticleController {

    private static final Logger LOGGER = Logger.getLogger(ArticleController.class.getName());
    
    @Autowired
    IArticleManagement articleManager;
    
    @Autowired
    IUserManagement userManager;
    
    @Autowired
    PrepareUIData prepareUIData;
    
    @ExceptionHandler(ArticleNotFoundException.class)
    public String handleArticleNotFoundException(Model model) {
        model.addAttribute("pageTitle", "Article inconnu");
        model.addAttribute("errorHeader", "Article inconnu");
        model.addAttribute("errorMsg", "L'article recherché n'existe pas ! Veuillez vérifier votre recherche ou le titre de l'article dans l'URL de la page actuel.");
        model.addAttribute("errorFooter", "L'article peut avoir changé de titre ou avoir été supprimé.");
        return "notFound";
    }
    
    @ExceptionHandler(InterviewNotFoundException.class)
    public String handleInterviewNotFoundException(Model model) {
        model.addAttribute("pageTitle", "Interview inconnu");
        model.addAttribute("errorHeader", "Interview inconnu");
        model.addAttribute("errorMsg", "L'interview recherché n'existe pas ! Veuillez vérifier votre recherche ou le titre de l'interview dans l'URL de la page actuel.");
        model.addAttribute("errorFooter", "L'interview peut avoir changé de titre ou avoir été supprimé.");
        return "notFound";
    }
    
    private Article getArticle(Optional<Article> optArticle, RuntimeException ex) {
        if (optArticle.isPresent()) {
            return optArticle.get();
        } else {
            LOGGER.log(Level.WARNING, ex.getMessage(), ex);
            throw ex;
        }
    }
    
    private Article getArticleRelatedToSlug(String slugArticle) {
        Optional<Article> optArticle = this.articleManager.findBySlug(slugArticle);
        return this.getArticle(optArticle, new ArticleNotFoundException("Unkown article for the slug : " + slugArticle));
    }
    
    private Article getInterviewRelatedToSlug(String slugArticle) {
        Optional<Article> optArticle = this.articleManager.findBySlug(slugArticle);
        return this.getArticle(optArticle, new ArticleNotFoundException("Unkown interview for the slug : " + slugArticle));
    }
    
    @GetMapping("/article/creation")
    public String creationArticle(Model model) {
        
        List<User> writters = userManager.findAll();
        if (writters.isEmpty()) {
            this.userManager.add("Test", "Rédacteur", "redac.test@rgpd.fr");
            this.userManager.add("Roger", "Jean", "jean.roger@rgpd.fr");
            writters = userManager.findAll();
        }
        
        model.addAttribute("users", writters);
        model.addAttribute("pageTitle", "Création d'article");

        return "publishArticle";
    }
    
    @PostMapping("/article/creation")
    public String validationArticle(
            @RequestParam(value = "title", required = true) String title,
            @RequestParam(value = "subTitle", required = false) String subTitle,
            @RequestParam(value = "keywords", required = false) String keywords,
            @RequestParam(value = "type", required = true) String typeArticle,
            @RequestParam(value = "category", required = false) String category,
            @RequestParam(value = "eventDate", required = false) String eventDate,
            @RequestParam(value = "authors", required = true) long[] authors,
            @RequestParam(value = "content", required = false) String content,
            @RequestParam(value = "nbrRefs", required = true) int nbrRefs,
            @RequestParam Map<String, String> paramsRefs, // Used to get all the dynamically created references
            @RequestParam(value = "createArticle", required = false) String createArticle,
            Model model
    ) {
        Article newArticle = this.articleManager.add(title, subTitle, keywords, typeArticle, category, eventDate, authors, content, nbrRefs, paramsRefs, createArticle);
        return "redirect:/article/lire/" + newArticle.getSlug();
    }
    
    @GetMapping("/article/modification/{articleSlug}")
    public String modificationArticle(@PathVariable(value="articleSlug") String slugArticle, Model model) {
      Article article = this.getArticleRelatedToSlug(slugArticle);
      // Build a list of all the tag's
      // description separated by a comma. 
      String tagsDescription = String.join(",",
              article.getTags()
              .stream()
              .map(Tag::getDescription)
              .collect(Collectors.toList()));
      List<User> allOtherAuthors = userManager.findAll();
      // Remove the authors of the
      // Remove the authors of the
      // article to find which
      // authors need to be selected.
      allOtherAuthors.removeAll(article.getUsers());
      model.addAttribute("allOtherAuthors", allOtherAuthors);
      model.addAttribute("tagsDescription", tagsDescription);
      model.addAttribute("article", article);
      model.addAttribute("pageTitle", "Modification d'article");
      return "modificationArticle";
    }
    
    @PostMapping("/article/modification/{articleSlug}")
    public String updateArticle(
            @PathVariable(value = "articleSlug") String slugArticle,
            @RequestParam(value = "title", required = true) String title,
            @RequestParam(value = "subTitle", required = false) String subTitle,
            @RequestParam(value = "keywords", required = false) String keywords,
            @RequestParam(value = "type", required = true) String typeArticle,
            @RequestParam(value = "category", required = false) String category,
            @RequestParam(value = "eventDate", required = false) String eventDate,
            @RequestParam(value = "authors", required = true) long[] authors,
            @RequestParam(value = "content", required = false) String content,
            @RequestParam(value = "nbrRefs", required = true) int nbrRefs,
            @RequestParam Map<String, String> paramsRefs, // Used to get all the dynamically created references
            @RequestParam(value = "createArticle", required = false) String createArticle,
            Model model
    ) {
        Article article = this.getArticleRelatedToSlug(slugArticle);
        article = this.articleManager.update(article, title, subTitle, keywords, typeArticle, category, eventDate, authors, content, nbrRefs, paramsRefs, createArticle);
        return "redirect:/article/lire/" + article.getSlug();
    }
    
    @RequestMapping("/article/suppression/{slugArticle}")
    public String deleteArticle(@PathVariable(value="slugArticle") String slugArticle) {
        Article articleToDelete = this.getArticleRelatedToSlug(slugArticle);
        this.articleManager.delete(articleToDelete);
        // TODO : message de retour !
        return "redirect:/article/actualites";
    }
    
    @RequestMapping("/article/lire/{slugArticle}")
    public String displayArticle(@PathVariable(value="slugArticle") String slugArticle, Model model) {
        model.addAttribute("article", this.getArticleRelatedToSlug(slugArticle));
        return "displayArticle";
    }

    @RequestMapping("/article/actualites")
    public String displayAllArticle(Model model) {
        List<Article> articles = this.articleManager.findAllByOrderByPublicationDateDesc();
        if (articles.isEmpty()) {
            model.addAttribute("msg", "Aucun articles à afficher.");
        } else {
            model.addAttribute("articles", articles);
            model.addAttribute("articlesContentPreview", this.prepareUIData.buildPreview(articles));
        }
        model.addAttribute("pageTitle", "Actualités");
        return "displayAllArticle";
    }
    
    @RequestMapping("/article/a-propos-du-rgpd/categories")
    public String displayAboutGDPRCategories(Model model) {
        String[] categoriesLabel = {
                "Notions clefs", "Présentation du RGPD", "Droits des personnes concernées",
                "Obligations des RT et ST", "Transferts hors UE", "Outils de conformité"
        };
        model.addAttribute("categories", this.prepareUIData.buildCategories(categoriesLabel, Article.MAX_SLUG_CHAR));
        model.addAttribute("pageActive", "a-propos-du-rgpd");
        return "categories";
    }
    
    @RequestMapping("/article/a-propos-du-rgpd/{slugArticle}")
    public String displayArticleCategory(@PathVariable(value="slugArticle") String slugArticle) {
        Article article = this.getArticleRelatedToSlug(slugArticle);
        return "redirect:/article/lire/" + article.getSlug();
    }
    
    @RequestMapping("/interview/categories")
    public String displayInterviewCategories(Model model) {
        String[] categoriesLabel = {
                "L'avis d'un DPO", "L'avis d'un gendarme", "L'avis d'un gérant d'entreprise",
                "L'avis d'un journaliste", "L'avis d'un avocat d'association", "L'avis d'un ingenieur informatique"
        };
        model.addAttribute("categories", this.prepareUIData.buildCategories(categoriesLabel, Article.MAX_SLUG_CHAR));
        model.addAttribute("pageActive", "interview");
        return "categories";
    }
    
    @RequestMapping("/interview/{slugArticle}")
    public String displayInterviewCategory(@PathVariable(value="slugArticle") String slugArticle) {
        Article article = this.getInterviewRelatedToSlug(slugArticle);
        return "redirect:/article/lire/" + article.getSlug();
    }
}
