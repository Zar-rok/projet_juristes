package org.gdpr.gdpr_website.controller;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.gdpr.gdpr_website.entity.User;
import org.gdpr.gdpr_website.exception.RessourceNotFoundException;
import org.gdpr.gdpr_website.manager.IUserManagement;
import org.gdpr.gdpr_website.service.PrepareUIData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/redacteur")
public class RedacteurController {
    
    private static final Logger LOGGER = Logger.getLogger(RedacteurController.class.getName());

    @Autowired
    IUserManagement userManager;

    @Autowired
    PrepareUIData prepareUIData;
    
    @ExceptionHandler(RessourceNotFoundException.class)
    public String handleWritterNotFoundException(Model model) {
        model.addAttribute("pageTitle", "Rédacteur inconnu");
        model.addAttribute("errorHeader", "Rédacteur inconnu");
        model.addAttribute("errorMsg", "Le rédacteur recherché n'existe pas !");
        return "notFound";
    }
    
    @RequestMapping("/profil/{redacteurFirstnameLastname}")
    public String displayRedacteur(
            @PathVariable(value="redacteurFirstnameLastname") String redacteurFirstnameLastname,
            Model model
    ) {
        String[] firstnameAndLastname = redacteurFirstnameLastname.split("-");
        Optional<User> optUser = this.userManager.findByLastnameAndFirstnameIgnoreCase(
                firstnameAndLastname[1],
                firstnameAndLastname[0]
        );
        
        if (optUser.isPresent()) {
            User user = optUser.get();
            model.addAttribute("author", user);
            model.addAttribute("articles", user.getArticles());
            model.addAttribute("articlesContentPreview", this.prepareUIData.buildPreview(user.getArticles()));
            return "redacteur";
        } else {
            RessourceNotFoundException rnf = new RessourceNotFoundException("author", firstnameAndLastname[0] + ", " + firstnameAndLastname[1]);
            LOGGER.log(Level.WARNING, rnf.getMessage(), rnf);
            throw rnf;
        }
    }
}
