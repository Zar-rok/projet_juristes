package org.gdpr.gdpr_website.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


@Entity
public class User {
	
	public User() {}
	
	public User(String lastname, String firstname, String mail) {
		super();
		this.lastname = lastname;
		this.firstname = firstname;
		this.mail = mail;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@Column(nullable = false, length = 30)
	private String lastname;
	
	@Column(nullable = false, length = 30)
	private String firstname;
	
	@Column(nullable = false, length = 80)
	private String mail;
	
	@ManyToMany
	@JoinTable(name = "user_article",
			joinColumns = @JoinColumn(name = "user_id"),
			inverseJoinColumns = @JoinColumn(name = "article_id")
	)
	private List<Article> articles = new ArrayList<>();

	public List<Article> getArticles() {
		return articles;
	}

	public void setArticles(List<Article> articles) {
		this.articles = articles;
	}
	
  public void addArticle(Article newArticle) {
      this.articles.add(newArticle);
  }

  public void removeArticle(Article article) {
    this.articles.remove(article);
  }
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}
	
	public String toString() {
		return "[User " + id + "] : " + lastname + ", " + firstname + ", " + mail;	
	}

}