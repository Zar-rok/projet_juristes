package org.gdpr.gdpr_website.entity;

import javax.persistence.*;
import javax.persistence.Entity;

@Entity
public class Reference {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = false)
    private int orderIndex;

    @Column(nullable = false, length = 1024)
    private String content;

    public Reference() {
    }

    public Reference(int index, String refContent) {
        this.orderIndex = index;
        this.content = refContent;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getOrderIndex() {
        return orderIndex;
    }

    public void setOrderIndex(int index) {
        this.orderIndex = index;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String refContent) {
        this.content = refContent;
    }
}
