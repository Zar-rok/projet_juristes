package org.gdpr.gdpr_website.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Tag {
	
	public Tag () {}
	
	public Tag(String description) {
		super();
		this.description = description;
	}
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@Column(nullable = false, length = 30, unique = true)
	private String description;


    @ManyToMany(cascade = {
			CascadeType.PERSIST,
			CascadeType.MERGE
	})
	@JoinTable(name = "tag_article",
			joinColumns = @JoinColumn(name = "tag_id"),
			inverseJoinColumns = @JoinColumn(name = "article_id")
	)
	private List<Article> tagArticles = new ArrayList<>();

    @ManyToMany(cascade = {
            CascadeType.PERSIST,
            CascadeType.MERGE
    })
    @JoinTable(name = "tag_glossary",
            joinColumns = @JoinColumn(name = "tag_id"),
            inverseJoinColumns = @JoinColumn(name = "glossary_id")
    )
    private List<Glossary> glossaryEntries = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Article> getArticles() {
        return tagArticles;
    }

    public void setArticles(List<Article> tagArticles) {
        this.tagArticles = tagArticles;
    }
    
    public void addArticle(Article newArticle) {
        this.tagArticles.add(newArticle);
    }

    public void removeArticle(Article article) {
        this.tagArticles.remove(article);
    }

    public List<Glossary> getGlossaryEntries() {
        return glossaryEntries;
    }

    public void setGlossaryEntries(List<Glossary> glossaryEntries) {
        this.glossaryEntries = glossaryEntries;
    }
}
