package org.gdpr.gdpr_website.entity;


import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Glossary {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    @Column(nullable = false, length = 30)
    private String entryLabel;

    @Column(nullable = false, length = 1000)
    private String description;


    @ManyToMany(mappedBy = "glossaryEntries")
    private List<Tag> glossaryTags = new ArrayList<>();

    public void addTag(Tag t){
        glossaryTags.add(t);
        t.getGlossaryEntries().add(this);
    }

    public void removeTag(Tag t){
        glossaryTags.remove(t);
        t.getGlossaryEntries().remove(this);
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEntryLabel() {
        return entryLabel;
    }

    public void setEntryLabel(String entryLabel) {
        this.entryLabel = entryLabel;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Tag> getGlossaryTags() {
        return glossaryTags;
    }

    public void setGlossaryTags(List<Tag> glossaryTags) {
        this.glossaryTags = glossaryTags;
    }
}
