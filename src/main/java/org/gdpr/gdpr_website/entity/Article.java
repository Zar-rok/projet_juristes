package org.gdpr.gdpr_website.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.*;

import com.github.slugify.Slugify;

@Entity
public class Article {

    public static final int MAX_SLUG_CHAR = 30;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = false, length = 100)
    private String title;

    @Column(nullable = true, length = 100)
    private String subTitle;

    @Column(nullable = false, length = 1048576)
    private String content;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Reference> references;
    
    @Column(nullable = false, length = 30)
    private String type;
    
    @Column(nullable = true, length = 30)
    private String category;

    @Column(nullable = false)
    private Boolean isDraft;

    @Column(nullable = false, length = Article.MAX_SLUG_CHAR, unique = true)
    private String slug;

    private Date publicationDate;

    private Date modificationDate;

    private Date eventDate;

    @ManyToMany(mappedBy = "articles")
    private List<User> users = new ArrayList<>();

    @ManyToMany(mappedBy = "tagArticles")
    private List<Tag> tags = new ArrayList<>();

    public Article() {
    }

    public Article(String title, String subTitle, String content, List<Reference> references, String type, String category, Boolean isDraft, Date publicationDate, Date modificationDate, Date eventDate) {
        super();
        this.title = title;
        Slugify slg = new Slugify();
        this.slug = slg.slugify(title.substring(0, Math.min(Article.MAX_SLUG_CHAR, title.length())));
        this.content = content;
        this.references = references;
        this.type = type;
        this.category = category;
        this.isDraft = isDraft;
        this.subTitle = subTitle;
        this.publicationDate = publicationDate;
        this.modificationDate = modificationDate;
        this.eventDate = eventDate;
    }
    
    public Article(String title, String subTitle, String content, String type, String category, Boolean isDraft, Date publicationDate) {
        this(title, subTitle, content, null, type, category, isDraft, publicationDate, null, null);
    }
    
    public Article(String title, String subTitle, String content, String type, String category, Boolean isDraft, Date publicationDate, Date eventDate) {
        this(title, subTitle, content, null, type, category, isDraft, publicationDate, null, eventDate);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
        Slugify slg = new Slugify();
        this.setSlug(slg.slugify(title));
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<Reference> getReferences() {
        return references;
    }

    public void setReferences(List<Reference> references) {
        this.references = references;
    }
    
    public void clearAndResetReferences(List<Reference> references) {
        this.references.clear();
        this.references.addAll(references);
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    
    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Boolean getIsDraft() {
        return isDraft;
    }

    public void setIsDraft(Boolean isDraft) {
        this.isDraft = isDraft;
    }

    public Date getPublicationDate() {
        return publicationDate;
    }

    public void setPublicationDate(Date publicationDate) {
        this.publicationDate = publicationDate;
    }

    public Date getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(Date modificationDate) {
        this.modificationDate = modificationDate;
    }

    public Date getEventDate() {
        return eventDate;
    }

    public void setEventDate(Date eventDate) {
        this.eventDate = eventDate;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public void addUser(User u) {
        users.add(u);
        u.addArticle(this);
    }

    public void addUsers(List<User> users) {
        for (int i = 0; i < users.size(); i++) {
            addUser(users.get(i));
        }
    }

    public void removeUser(User u) {
        users.remove(u);
        u.getArticles().remove(this);
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public void addTag(Tag t) {
        tags.add(t);
        t.getArticles().add(this);
    }

    public void addTags(List<Tag> tags) {
        for (int i = 0; i < tags.size(); i++) {
            addTag(tags.get(i));
        }
    }

    public void removeTag(Tag t) {
        tags.remove(t);
        t.getArticles().remove(this);
    }
    
    public String getSlug() {
        return slug;
    }

    private void setSlug(String slug) {
        this.slug = slug;
    }
}
