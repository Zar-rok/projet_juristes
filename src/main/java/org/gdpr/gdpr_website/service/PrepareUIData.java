package org.gdpr.gdpr_website.service;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import org.gdpr.gdpr_website.entity.Article;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.stereotype.Service;

import com.github.slugify.Slugify;

@Service
public class PrepareUIData {

    private Slugify slg;
    
    public PrepareUIData() {
        this.slg = new Slugify();
    }
    
    public HashMap<String, String> buildCategories(String[] categoriesLabel, int maxSlugChar) {
        String value;
        String category;
        HashMap<String, String> categories = new HashMap<>(categoriesLabel.length);
        for (String aCategoriesLabel : categoriesLabel) {
            category = aCategoriesLabel;
            value = this.slg.slugify(category.substring(0, Math.min(maxSlugChar, category.length())));
            categories.put(category, value);
        }
        return categories;
    }

    private String buildPreviewString(String content) {
        return Jsoup.parse(content).select("p").first().text();
    }

    public List<String> buildPreview(List<Article> articles) {
        return articles.stream()
                .map(Article::getContent)
                .collect(Collectors.toList())
                .stream()
                .map(this::buildPreviewString)
                .collect(Collectors.toList());
    }
    
}
