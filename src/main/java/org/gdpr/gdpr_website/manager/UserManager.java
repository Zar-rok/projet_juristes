package org.gdpr.gdpr_website.manager;

import java.util.List;
import java.util.Optional;

import org.gdpr.gdpr_website.entity.User;
import org.gdpr.gdpr_website.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserManager implements IUserManagement {
	
	@Autowired
	UserRepository userRepo;
	
	public UserManager( ) { }
	
	public User add(String lastname, String firstname, String mail) {
	    User newUser = new User(lastname, firstname, mail);
	    return this.userRepo.save(newUser);
	}
	
	public List<User> findAll() {
	    return (List<User>) this.userRepo.findAll();
	}
	
	public void deleteAll() {
		userRepo.deleteAll();
	}
	
	public Optional<User> findByLastnameAndFirstnameIgnoreCase(String lastname, String firstname) {
	    return this.userRepo.findByLastnameAndFirstnameIgnoreCase(lastname, firstname);
	}
}
