package org.gdpr.gdpr_website.manager;

import java.util.List;
import java.util.Optional;

import org.gdpr.gdpr_website.entity.Tag;

public interface ITagManagement {
    public Optional<Tag> findByDescription(String description);
    public List<Tag> findAllByDescriptionIgnoreCaseContaining(String description);
	public Tag add(String description);
}
