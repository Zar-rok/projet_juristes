package org.gdpr.gdpr_website.manager;

import java.util.List;
import java.util.Optional;

import org.gdpr.gdpr_website.entity.User;

public interface IUserManagement {

    public User add(String lastname, String firstname, String mail);
    public List<User> findAll();
    public void deleteAll();
    public Optional<User> findByLastnameAndFirstnameIgnoreCase(String lastname, String firstname);
}
