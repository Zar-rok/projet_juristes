package org.gdpr.gdpr_website.manager;

import org.gdpr.gdpr_website.entity.Reference;

public interface IReferenceManagement {

    public Reference addReference(int index, String content);
}
