package org.gdpr.gdpr_website.manager;

import org.gdpr.gdpr_website.entity.Reference;
import org.gdpr.gdpr_website.repository.ReferenceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ReferenceManager implements IReferenceManagement {

    @Autowired
    ReferenceRepository refRepo;

    public Reference addReference(int index, String content) {
        Reference r = new Reference(index, content);
        return refRepo.save(r);
    }
}
