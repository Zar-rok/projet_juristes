package org.gdpr.gdpr_website.manager;

import org.gdpr.gdpr_website.repository.GlossaryRepository;
import org.gdpr.gdpr_website.repository.TagRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GlossaryManager implements IGlossaryManagement {

	@Autowired
	GlossaryRepository glossaryRepo;
	
	@Autowired
	TagRepository tagRepo;
	
	public GlossaryManager () { } 
}
