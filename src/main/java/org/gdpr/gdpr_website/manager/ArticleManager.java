package org.gdpr.gdpr_website.manager;

import org.gdpr.gdpr_website.entity.Reference;
import org.gdpr.gdpr_website.repository.ReferenceRepository;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.gdpr.gdpr_website.entity.Article;
import org.gdpr.gdpr_website.entity.Tag;
import org.gdpr.gdpr_website.entity.User;
import org.gdpr.gdpr_website.repository.ArticleRepository;
import org.gdpr.gdpr_website.repository.TagRepository;
import org.gdpr.gdpr_website.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;

@Service
public class ArticleManager implements IArticleManagement {

  private static final Logger LOGGER = Logger.getLogger(ArticleManager.class.getName());

	@Autowired
	ArticleRepository articleRepo;
	
	@Autowired
	UserRepository userRepo;
	
	@Autowired
	TagRepository tagRepo;

	@Autowired
	ReferenceRepository refRepo;
	
	public ArticleManager ( ) { }
	
	private static List<String> retrieveReferences(int nbrRefs, Map<String, String> paramsRefs) {
      String currentRefDesc;
      List<String> refsDescriptions = new ArrayList<>();
      for (int i = 1; i <= nbrRefs; ++i) {
          currentRefDesc = paramsRefs.get("reference" + i);
          if (currentRefDesc != null && !currentRefDesc.isEmpty()) {
              refsDescriptions.add(currentRefDesc);
          }
      }
      return refsDescriptions;
	}
	
	private static long[] toPrimitive(List<Long> values) {
	    long[] primitiveValues = new long[values.size()];
	    for (int i = 0; i < primitiveValues.length; ++i) {
	        primitiveValues[i] = values.get(i);
	    }
	    return primitiveValues;
	}
	
	private List<Reference> initReferences(List<String> references) {
      List<Reference> newReferences = new ArrayList<>();
      for (int i = 0; i < references.size(); ++i) {
          Reference newReference = new Reference(i+1, references.get(i));
          this.refRepo.save(newReference);
          newReferences.add(newReference);
      }
      return newReferences;
	}
	
	private void createAndLinkReferences(Article article, List<String> references) {
      // OneToMany between Article and Reference
      // force to save the instance of the new article
      // before linking with the references
      articleRepo.save(article);
      List<Reference> newReferences = this.initReferences(references);
      article.setReferences(newReferences);
	}
	
  private void updateAndCreateAndLinkReferences(Article article, List<String> references) {
      // OneToMany between Article and Reference
      // force to save the instance of the new article
      // before linking with the references
      articleRepo.save(article);
      List<Reference> newReferences = this.initReferences(references);
      article.clearAndResetReferences(newReferences);
  }
	
	private void unlinkTags(Article article, List<String> tagsToRemove) {
	    for (String description : tagsToRemove) {
          Optional<Tag> optTag = this.tagRepo.findByDescription(description);
          if (optTag.isPresent()) {
              article.removeTag(optTag.get());
          }
      }
	}
	
	private void linkTags(Article article, String[] tagsDescriptions) {
	    Tag currentTag;
      for (String description : tagsDescriptions) {
          Optional<Tag> optTag = this.tagRepo.findByDescription(description);
          if (optTag.isPresent()) {
              currentTag = optTag.get();
          } else {
              if (!description.isEmpty()) {
                  currentTag = new Tag(description);
                  this.tagRepo.save(currentTag);
              } else {
                  continue;
              }
          }
          article.addTag(currentTag);
        }
	}
	
  private void unlinkUsers(Article article, List<Long> authorsIds) {
      for (long authorId : authorsIds) {
          Optional<User> optUser = userRepo.findById(authorId);
          if (optUser.isPresent()) {
              article.removeUser(optUser.get());
          }
      }
  }
	
	private void linkUsers(Article article, long[] authorsIds) {
      HashSet<User> userSet = new HashSet<User>();
      for (long authorId : authorsIds) {
          Optional<User> optUser = userRepo.findById(authorId);
          if (optUser.isPresent()) {
              userSet.add(optUser.get());
          }
      }
      
      Iterator<User> userIterator = userSet.iterator();
      while(userIterator.hasNext()) {
        article.addUser(userIterator.next());
      }
	}
	
	private Article linkUsersTagsRefsAndSave(Article newArticle, List<String> references, long[] authorsIds, String[] tagsDescriptions) {
	    this.createAndLinkReferences(newArticle, references);
	    this.linkUsers(newArticle, authorsIds);
      this.linkTags(newArticle, tagsDescriptions);
	    return articleRepo.save(newArticle);
	}
	
	private Article updateLinkUsersTagsRefsAndSave(Article article, String title, String subTitle, String content, List<String> references, String type, String category, Boolean isDraft, long[] authorsIds, String[] tagsDescriptions) {
      
      this.updateAndCreateAndLinkReferences(article, references);
      
      List<String> currentTags = article.getTags().stream().map(Tag::getDescription).collect(Collectors.toList());
      List<String> currentTagsCopy = new ArrayList<String>(currentTags);
      List<String> updatedTags = Arrays.stream(tagsDescriptions).collect(Collectors.toList());
      List<String> updatedTagsCopy = new ArrayList<String>(updatedTags);
      
      currentTagsCopy.removeAll(updatedTags); // Tags to remove
      updatedTagsCopy.removeAll(currentTags); // Tags to add
      
      this.unlinkTags(article, currentTagsCopy);
      Object[] tagsToAdd = updatedTagsCopy.toArray();
      this.linkTags(article, Arrays.copyOf(tagsToAdd, tagsToAdd.length, String[].class));
      
      List<Long> currentUsers = article.getUsers().stream().map(User::getId).collect(Collectors.toList());
      List<Long> currentUsersCopy = new ArrayList<Long>(currentUsers);
      List<Long> updatedUsers = new ArrayList<Long>(authorsIds.length);
      for (Long id : authorsIds) {
          updatedUsers.add(id);
      }
      List<Long> updatedUsersCopy = new ArrayList<Long>(updatedUsers);
      currentUsersCopy.removeAll(updatedUsers); // Users to remove
      updatedUsersCopy.removeAll(currentUsers); // Users to add
      
      this.unlinkUsers(article, currentUsersCopy);
      this.linkUsers(article, ArticleManager.toPrimitive(updatedUsersCopy));
      
      // TODO : Modify slug if title change ?
      article.setTitle(title);
      article.setSubTitle(subTitle);
      article.setContent(content);
      article.setType(type);
      article.setCategory(category);
      article.setIsDraft(isDraft);
      
	    return this.articleRepo.save(article);
	}
	
  public Optional<Article> findBySlug(String slug) {
      return this.articleRepo.findBySlug(slug);
  }
  
  public List<Article> findAllByOrderByPublicationDateDesc() {
      return this.articleRepo.findAllByOrderByPublicationDateDesc();
  }
  
  public Optional<Article> findByTypeAndCategory(String type, String category) {
      return this.articleRepo.findByTypeAndCategory(type, category);
  }
  
  public List<Article> findAll() {
    return (List<Article>) this.articleRepo.findAll();
  }
  
  public List<Article> findAllArticle() {
    List<Article> articles = new ArrayList<>();
    this.articleRepo.findAll().forEach(articles::add);
    return articles;
  }

    public List<Article> findAllByTitleIgnoreCaseContaining(String title) {
	    return this.articleRepo.findAllByTitleIgnoreCaseContaining(title);
    }
	
  public Article add(String title, String subTitle, String keywords, String typeArticle, String category, String eventDate, long[] authors, String content, int nbrRefs, Map<String, String> paramsRefs, String createArticle) {

    List<String> references = ArticleManager.retrieveReferences(nbrRefs, paramsRefs);
    boolean isDraft = createArticle.equals("draft");
    String[] tagsDescriptions = keywords.split(",");
    
    Article newArticle = new Article(title, subTitle, content, typeArticle, category, isDraft, new Date());
    if (typeArticle.equals("4")) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date eventRealDate = formatter.parse(eventDate);
            newArticle.setEventDate(eventRealDate);
        } catch (ParseException e) {
            LOGGER.log(Level.WARNING, e.getMessage(), e);
            throw new RuntimeException("TODO : gestion des erreurs !");
        }
    }
    
		return this.linkUsersTagsRefsAndSave(newArticle, references, authors, tagsDescriptions);
	}
  
  public Article update(Article article, String title, String subTitle, String keywords, String typeArticle, String category, String eventDate, long[] authors, String content, int nbrRefs, Map<String, String> paramsRefs, String createArticle) {
      
      List<String> references = ArticleManager.retrieveReferences(nbrRefs, paramsRefs);
      boolean isDraft = createArticle.equals("draft");
      System.out.println("keywords : " + keywords);
      String[] tagsDescriptions = keywords.split(",");
      
      if (typeArticle.equals("4")) {
          SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
          try {
              Date eventRealDate = formatter.parse(eventDate);
              article.setEventDate(eventRealDate);
          } catch (ParseException e) {
              LOGGER.log(Level.WARNING, e.getMessage(), e);
              throw new RuntimeException("TODO : gestion des erreurs !");
          }
      }
      
      return this.updateLinkUsersTagsRefsAndSave(article, title, subTitle, content, references, typeArticle, category, isDraft, authors, tagsDescriptions);
  }

  public void delete(Article article) {
      this.articleRepo.delete(article);
  }	
	
	public void deleteAll() {
		articleRepo.deleteAll();
	}
}
