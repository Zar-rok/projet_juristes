package org.gdpr.gdpr_website.manager;

import java.util.List;
import java.util.Optional;

import org.gdpr.gdpr_website.entity.Tag;
import org.gdpr.gdpr_website.repository.GlossaryRepository;
import org.gdpr.gdpr_website.repository.TagRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TagManager implements ITagManagement {

	@Autowired
	TagRepository tagRepo;
	
	@Autowired
	GlossaryRepository glossaryRepo;
	
	public TagManager ( ) {
	    
	}
	
	public Optional<Tag> findByDescription(String description) {
	    return this.tagRepo.findByDescription(description);
  }

	public List<Tag> findAllByDescriptionIgnoreCaseContaining(String description) {
		return this.tagRepo.findAllByDescriptionIgnoreCaseContaining(description);
	}

	public Tag add(String description) {
		Tag newTag = new Tag(description);
		return this.tagRepo.save(newTag);
	}
}
