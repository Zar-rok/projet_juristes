package org.gdpr.gdpr_website.manager;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.gdpr.gdpr_website.entity.Article;

public interface IArticleManagement {
    
	/* findAllArticle does more things, do not delete either of those functions */
    public List<Article> findAll();
    public List<Article> findAllArticle();
    public Optional<Article> findBySlug(String slug);
    public List<Article> findAllByOrderByPublicationDateDesc();
    public Optional<Article> findByTypeAndCategory(String type, String category);
    public List<Article> findAllByTitleIgnoreCaseContaining(String title);

    public Article add(String title, String subTitle, String keywords, String typeArticle, String category, String eventDate, long[] authors, String content, int nbrRefs, Map<String, String> paramsRefs, String createArticle);
    public Article update(Article article, String title, String subTitle, String keywords, String typeArticle, String category, String eventDate, long[] authors, String content, int nbrRefs, Map<String, String> paramsRefs, String createArticle);
    public void delete(Article article);
    public void deleteAll();
}
