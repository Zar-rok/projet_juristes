package org.gdpr.gdpr_website.manager_test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.gdpr.gdpr_website.RgpdWebsiteApplication;
import org.gdpr.gdpr_website.entity.User;
import org.gdpr.gdpr_website.manager.IUserManagement;
import org.gdpr.gdpr_website.manager.UserManager;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = RgpdWebsiteApplication.class)
public class UserManagerTest {
	
	@Autowired
    IUserManagement um;
	@Test
	public void emptyFindAllCall() {
		assertTrue(this.um.findAll().isEmpty());
	}
	
	@Test
	public void addUserTest() {
		
		User reference = new User("Doe", "John", "john.doe@gdpr.org");
		
		
		User added = this.um.add("Doe", "John", "john.doe@gdpr.org");

		//Return value
		assertEquals(added.getLastname(), reference.getLastname());
		assertEquals(added.getFirstname(), reference.getFirstname());
		assertEquals(added.getMail(), reference.getMail());
		
		
		//Fetched from database
		
		added = um.findAll().get(0);
		
		assertEquals(added.getLastname(), reference.getLastname());
		assertEquals(added.getFirstname(), reference.getFirstname());
		assertEquals(added.getMail(), reference.getMail());
		
		//Cleanup
		um.deleteAll();
	}
	
	

}
