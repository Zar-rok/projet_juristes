package org.gdpr.gdpr_website.manager_test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Optional;

import org.gdpr.gdpr_website.RgpdWebsiteApplication;
import org.gdpr.gdpr_website.entity.Article;
import org.gdpr.gdpr_website.entity.User;
import org.gdpr.gdpr_website.manager.IArticleManagement;
import org.gdpr.gdpr_website.manager.ITagManagement;
import org.gdpr.gdpr_website.manager.IUserManagement;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = RgpdWebsiteApplication.class)
@Transactional //Auto-manage session, useful for anything relating to database fetching
public class ArticleManagerTest {
	
	@Autowired
	IArticleManagement am;
	
	@Autowired
	IUserManagement um;
	
	@Autowired
	ITagManagement tm;
	
	@Test
	public void addArticleTest() {
		
		//Convenience variables
		String title = "Article test";
		String subtitle = "A test article";
		String content = 
				"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur at augue finibus, mollis diam ut, sollicitudin turpis. Aliquam imperdiet magna vitae velit consectetur, et dapibus magna tempus. Praesent elementum velit sapien, sit amet efficitur dui cursus posuere. Sed consequat lacinia leo id rhoncus. Mauris vitae venenatis tortor, in suscipit leo. Integer condimentum sed erat ut congue. Mauris quis viverra sapien, eu luctus ex. Curabitur at elementum eros. Nunc mollis aliquam sem ut euismod.\n"
				;
		
		//Add a user to cover the relevant "if" in ArticleManager
		User usr = um.add("Doe", "John", "john.doe@test.com");
		
		long[] authorsId = {usr.getId(), 12};
		
		//Add a tag to cover the relevant "if" in ArticleManager
		tm.add("rgpd");
		
		String keywords = "rgpd,test,news";
		
		String typeArticle = "1";
		
		HashMap<String, String> paramsRefs = new HashMap<String, String>();
		paramsRefs.put("reference0", "Bonjour");
		paramsRefs.put("reference1", "Coucou!");
		
		//Add in database
		Article articleAdded = am.add(
				"Article test",
				"A test article",
				keywords,
				typeArticle,
				"",
				"",
				authorsId,
				"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur at augue finibus, mollis diam ut, sollicitudin turpis. Aliquam imperdiet magna vitae velit consectetur, et dapibus magna tempus. Praesent elementum velit sapien, sit amet efficitur dui cursus posuere. Sed consequat lacinia leo id rhoncus. Mauris vitae venenatis tortor, in suscipit leo. Integer condimentum sed erat ut congue. Mauris quis viverra sapien, eu luctus ex. Curabitur at elementum eros. Nunc mollis aliquam sem ut euismod.\n",
				2,
				paramsRefs,
				"publish"
				);
		
		assertNotNull(articleAdded);
		
		//Fetch the database result in another object
		Article dbArticleAdded = am.findAll().get(0);
		
		//Check that data is valid
		assertEquals(title, dbArticleAdded.getTitle());
		assertEquals(subtitle, dbArticleAdded.getSubTitle());
		assertEquals(content, dbArticleAdded.getContent());
		
		assertEquals(usr.getId(), dbArticleAdded.getUsers().get(0).getId());
		System.out.println(dbArticleAdded.getUsers());
		assertEquals(1, dbArticleAdded.getUsers().size());
		
		assertEquals("1", dbArticleAdded.getType());
		assertEquals(false, dbArticleAdded.getIsDraft());
	
		//Check that the function result is identical 
		assertEquals(articleAdded.getTitle(), dbArticleAdded.getTitle());
		assertEquals(articleAdded.getSubTitle(), dbArticleAdded.getSubTitle());
		assertEquals(articleAdded.getContent(), dbArticleAdded.getContent());
		assertEquals(articleAdded.getType(), dbArticleAdded.getType());
		assertEquals(articleAdded.getIsDraft(), dbArticleAdded.getIsDraft());
		
		assertEquals("Bonjour", articleAdded.getReferences().get(0).getContent());
		
		am.deleteAll();
	}
	
	@Test
	public void addArticleWithDuplicateAuthorsIds() {
		ArrayList<String> references = new ArrayList<>();
		references.add("testRef");
		
		//Add a user to cover the relevant "if" in ArticleManager
		User usr = um.add("Doe", "John", "john.doe@test.com");
		
		long[] authorsId = {usr.getId(), usr.getId()};
		
		//Add a tag to cover the relevant "if" in ArticleManager
		tm.add("rgpd");
		
		String [] tagsDescription = { "rgpd", "test", "news"};
		
    String keywords = "rgpd,test,news";
    
    String typeArticle = "1";
    
    HashMap<String, String> paramsRefs = new HashMap<String, String>();
    paramsRefs.put("reference0", "Bonjour");
    paramsRefs.put("reference1", "Coucou!");
    
    //Add in database
    Article articleAdded = am.add(
        "Article test",
        "A test article",
        keywords,
        typeArticle,
        "",
        "",
        authorsId,
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur at augue finibus, mollis diam ut, sollicitudin turpis. Aliquam imperdiet magna vitae velit consectetur, et dapibus magna tempus. Praesent elementum velit sapien, sit amet efficitur dui cursus posuere. Sed consequat lacinia leo id rhoncus. Mauris vitae venenatis tortor, in suscipit leo. Integer condimentum sed erat ut congue. Mauris quis viverra sapien, eu luctus ex. Curabitur at elementum eros. Nunc mollis aliquam sem ut euismod.\n",
        2,
        paramsRefs,
        "publish"
        );
		
		//Fetch the database result in another object
		Article dbArticleAdded = am.findAll().get(0);
		
		assertEquals(usr.getId(), dbArticleAdded.getUsers().get(0).getId());
		System.out.println(dbArticleAdded.getUsers());
		assertEquals(1, dbArticleAdded.getUsers().size());
	}
	
	@Test
	public void FindArticleBySlugTest() {
		
		ArrayList<String> references = new ArrayList<>();
		references.add("testRef");
		
		//Add a user to cover the relevant "if" in ArticleManager
		User usr = um.add("Doe", "John", "john.doe@test.com");
		
		long[] authorsId = {usr.getId(), usr.getId()};
		
		//Add a tag to cover the relevant "if" in ArticleManager
		tm.add("rgpd");
		
    String keywords = "rgpd,test,news";
    
    String typeArticle = "1";
    
    HashMap<String, String> paramsRefs = new HashMap<String, String>();
    paramsRefs.put("reference0", "Bonjour");
    paramsRefs.put("reference1", "Coucou!");
    
    //Add in database
    Article articleAdded = am.add(
        "Article test",
        "A test article",
        keywords,
        typeArticle,
        "",
        "",
        authorsId,
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur at augue finibus, mollis diam ut, sollicitudin turpis. Aliquam imperdiet magna vitae velit consectetur, et dapibus magna tempus. Praesent elementum velit sapien, sit amet efficitur dui cursus posuere. Sed consequat lacinia leo id rhoncus. Mauris vitae venenatis tortor, in suscipit leo. Integer condimentum sed erat ut congue. Mauris quis viverra sapien, eu luctus ex. Curabitur at elementum eros. Nunc mollis aliquam sem ut euismod.\n",
        2,
        paramsRefs,
        "publish"
        );
		
		//Fetch the database result in another object
		Optional<Article> dbArticleAdded = am.findBySlug(articleAdded.getSlug());
		
		//Assert it was fetched and is the same article
		assertTrue(dbArticleAdded.isPresent());
		assertEquals(articleAdded.getId(), dbArticleAdded.get().getId());
	}

}
